<!DOCTYPE html>
<html lang="en">
<head>
 <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
 <title>ATN Medicare - Medical Aid Beyond Barriers !!!</title>    
 

<link rel="stylesheet" type="text/css"
	href="./bootstrap/css/bootstrap-theme.min.css" />
<link rel="stylesheet" type="text/css"
	href="./bootstrap/css/bootstrap-theme.min.css.map" />
<link rel="stylesheet" type="text/css"
	href="./bootstrap/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css"
	href="./bootstrap/css/bootstrap.min.css.map" />
<link rel="stylesheet" type="text/css"
	href="./bootstrap/css/normalize.css" />
<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans" />

<script src="./bootstrap/js/jquery-2.2.3.min.js"></script>
<script src="./bootstrap/js/bootstrap.min.js"></script>

<script src="./bootstrap/js/require.js"></script>
<script src="./bootstrap/js/collapse.js"></script>
<script src="./bootstrap/js/dropdown.js"></script>
<script src="./bootstrap/js/transition.js"></script>


<!-- <script type="text/javascript">
$(document).bind("mobileinit", function () {
    $.mobile.hashListeningEnabled = false;
    $.mobile.pushStateEnabled = false;
    $.mobile.changePage.defaults.changeHash = false;
});

</script> -->
 
 
  
<style type="text/css">

html,body,h1,h2,h3,h4,h5,h6,p,span {
	font-family: 'Open Sans', sans-serif!important;
 	font-weight: 400!important;
}
</style>

<style type="text/css">
.jumbotron {
	position: relative;
  /*background: #000 url("./images/landscape.jpg") center center;*/ 
  background: #000 url("./images/s1.png") center center; 
   /*  background: #000 url("./images/4.png") center center; */
    background-size: 100% 100%;
    -moz-background-size: 100% 100%;
    -webkit-background-size: 100% 100%;
    -o-background-size: 100% 100%;
    height: 350px;
        overflow: hidden;
        border-bottom: 1px solid #e1e1e1;
 	margin-bottom: 0;
}

</style>

</head>

<body>





<header class="">
<nav class="navbar navbar-fixed-top" style="background-color: #FFF; margin-bottom: 0; border: 1px solid #E1E1E1;">
		<div class="container-fluid">

			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"
					aria-expanded="false">
					<span class="sr-only" style="background-color: #000;">Toggle navigation</span>
					 <span class="icon-bar" style="background-color: #000;"></span>
						 <span class="icon-bar" style="background-color: #000;"></span> 
						 <span class="icon-bar" style="background-color: #000;"></span>
				</button>


				<a class="navbar-brand" href="javascript:void(0);"
					style="margin: 0;"> <img alt="Brand"
					src="./images/MEDICARE1.png" style="margin: -12px;" width="120px"
					height="45px">

				</a>
			
			</div>
	<div class="navbar-header" style="display:table; padding-top: 12px;padding-bottom: 6px;"><p class="lead" style="display:table-cell;padding-left:0.5em; font-size:1.3em;color: #fcd309;font-weight: bold"><font style="color:#EB2728">Medical</font> <font style="color:#1B8F48">Aid</font> ~ <font style="color:#2F2F79">Beyond </font>Barriers</p></div>
			
			<div class="navbar-right collapse navbar-collapse"
				id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav" style="color: #000">
					<li class="active" style="font-weight:bold;color: #1d904a;"><a href="./home.jsp" class="text-success">Home</a></li>
					<li><a href="./whyKolkata.jsp" class="text-success">Why Kolkata?</a></li>
					<li><a href="./services.jsp" class="text-success">Services</a></li>
					<li><a href="./contact_us.jsp" class="text-success">Contact us</a></li>
					<li><a href="http://atnmedicare.com/m/#/clientReg" class="text-success">Register</a></li>
					<li><a href="http://atnmedicare.com/m/#/login" class="text-success">Login</a></li>
				</ul>

			</div>
		</div>
	</nav>

</header>
	


	<div class="jumbotron">
		<!-- <div class="container text-center" style="background: #E1E1E1; bottom: 0; position: absolute;   margin: auto;    width: 100%;">
		
			<a class="btn btn-link btn-lg" href="#" role="button">Sign Up</a>
		
		</div> -->
		
	</div>
	<div class="container-fluid" style="margin-bottom: 4em;">

		<div class="page-header" style="margin: 0;">
			<h1 class="text-primary"></h1>
			<h2 class="text-success" style="font-size:1.8em; font-weight: bold">
				 About Us
			</h2>
		</div>

		<div class="panel panel-default" style="border: none;">
		    <p class="lead" style="padding:15px 0px  0px 0px;font-size:1.3em">ATN Medicare, is a comprehensive end-to-end health and medical service and utility project for the people of Bangladesh, enabling them to access the world class medical facilities in and around Kolkata. The registered users of ATN Medicare would be entitled to privileged services like Appointment Scheduling, Consultation, Doctor visit, Tests, Hospitalization, Pre and Post Surgery along with Operative Treatment. Further, to add ATN Medicare also helps in Visa Processing, Travel and Accommodation planning with facilities as per the requirement of the Client.</p>
		<p class="lead" style="padding:  0px 0px 0px 0px; font-size:1.3em">ATN Medicare co ordinate such services with trust and dependability due to its personal and close relationship with the leading Doctors&#39;, Medical Centre, Pathological Labs and Research Institutes.</p>
		
		</div>
		
		<div class="page-header" style="margin: 0;">
			<h2 class="text-success " style="font-size:1.8em; font-weight:  bold">
				Mission
			</h2>
		</div>

		<div class="panel panel-default" style="border: none;">
		<p class="lead" style="padding:15px 0px  0px 0px; font-size:1.3em;">ATN Medicare&#39;s prime object is to bring world class health services to the people of Bangladesh, through emotional trust factor between Bangladesh and Kolkata, facilitating safe and sound treatment along with hassle free processing.</p>
		<p class="lead" style="padding:0px 0px;font-size:1.3em;">ATN Medicare understands very well about that the entire procedure and considers that to provide try to satisfaction to the Patients&#39; and their family.</p>
		</div>
		
		<div class="page-header" style="margin: 0;">
			<h2 class="text-success " style="font-size:1.8em; font-weight:  bold">
				Vision
			</h2>
		</div>

		<div class="panel panel-default" style="border: none;">
		    <p class="lead" style="padding:15px 0px;font-size:1.3em">ATN Medicare&#39;s vision is to consider highest care with top priority for human life with motto as &#34;Service to one and all with a Spirit of Trust and Security&#34;.</p>
		
		</div>
 
 

	</div>

	

	<footer class="footer navbar-fixed-bottom" style="border-top: 1px solid #E1E1E1; background-color: #EEEEEE;">
      <div class="container-fluid" style="display:table; margin-top:7px;padding-bottom: 0px;width:100%">
       <p class="lead pull-left" style="display:table-cell"><small>Copyright &copy; 2016</small></p>
        <p class="lead pull-right" style="display:table-cell"><small>ATN Medicare</small></p>
      </div>
    </footer>

</body>




</html>
