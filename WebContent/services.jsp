<!DOCTYPE html>
<html lang="en">
<head>
 <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
 <title>ATN Medicare - Our Services for You !!!</title>    
<link rel="stylesheet" type="text/css"
	href="./bootstrap/css/bootstrap-theme.min.css" />
<link rel="stylesheet" type="text/css"
	href="./bootstrap/css/bootstrap-theme.min.css.map" />
<link rel="stylesheet" type="text/css"
	href="./bootstrap/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css"
	href="./bootstrap/css/bootstrap.min.css.map" />
<link rel="stylesheet" type="text/css"
	href="./bootstrap/css/normalize.css" />
<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans" />

<script src="./bootstrap/js/jquery-2.2.3.min.js"></script>
<script src="./bootstrap/js/bootstrap.min.js"></script>

<script src="./bootstrap/js/require.js"></script>
<script src="./bootstrap/js/collapse.js"></script>
<script src="./bootstrap/js/dropdown.js"></script>
<script src="./bootstrap/js/transition.js"></script>

<style type="text/css">
html,body,h1,h2,h3,h4,h5,h6,p,span {
	font-family: 'Open Sans', sans-serif!important;
 	font-weight: 400!important;
}
.jumbotron {
	position: relative;
    /* background: #000 url("./images/landscape.jpg") center center; */
      background: #000 url("./images/s3.png") center center; 
    background-size: 100% 100%;
    -moz-background-size: 100% 100%;
    -webkit-background-size: 100% 100%;
    -o-background-size: 100% 100%;
    height: 350px;
        overflow: hidden;
        border-bottom: 1px solid #e1e1e1;
 	margin-bottom: 0;
}

</style>

</head>

<body>
<header class="">
<nav class="navbar navbar-fixed-top" style="background-color: #FFF; margin-bottom: 0; border: 1px solid #E1E1E1;">
		<div class="container-fluid">

			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"
					aria-expanded="false">
					<span class="sr-only" style="background-color: #000;">Toggle navigation</span>
					 <span class="icon-bar" style="background-color: #000;"></span>
						 <span class="icon-bar" style="background-color: #000;"></span> 
						 <span class="icon-bar" style="background-color: #000;"></span>
				</button>


				<a class="navbar-brand" href="javascript:void(0);"
					style="margin: 0;"> <img alt="Brand"
					src="./images/MEDICARE1.png" style="margin: -12px;" width="120px"
					height="45px">

				</a>

			</div>

<div class="navbar-header" style="display:table; padding-top: 12px;padding-bottom: 6px;"><p class="lead" style="display:table-cell;padding-left:0.5em; font-size:1.3em;color: #fcd309;font-weight: bold"><font style="color:#EB2728">Medical</font> <font style="color:#1B8F48">Aid</font> ~ <font style="color:#2F2F79">Beyond </font>Barriers</p></div>
			<div class="navbar-right collapse navbar-collapse"
				id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav" style="color: #000">
					<li class="active"><a href="./home.jsp" class="text-success">Home </a></li>
					<li ><a href="./whyKolkata.jsp" class="text-success">Why Kolkata?</a></li>
					<li class="active" style="font-weight:bold;color: #1d904a;"><a href="./services.jsp" class="text-success">Services</a></li>
					<li><a href="./contact_us.jsp" class="text-success">Contact us</a></li>
					<li><a href="http://atnmedicare.com/m/#/clientReg" class="text-success">Register</a></li>
					<li><a href="http://atnmedicare.com/m/#/login" class="text-success">Login</a></li>
				</ul>
			</div>
		</div>
	</nav>

</header>
	


	<div class="jumbotron">
		<!-- <div class="container text-center" style="background: #E1E1E1; bottom: 0; position: absolute;   margin: auto;    width: 100%;">
		
			<a class="btn btn-link btn-lg" href="#" role="button">Sign Up</a>
		
		</div> -->
		
	</div>
	<div class="container-fluid" style="margin-bottom: 4em;">

		<div class="page-header" style="margin: 0;">
			<h2 class="text-success" style="font-size:1.8em; font-weight: bold">
				 Medical Services
			</h2>
		</div>

		<div class="panel panel-default" style="border: none;">
		   
					<ul style="list-style-type:square" >
					<li style="padding:  15px 0px 0px 0px;"><b>Doctor&#39;s recommendations :</b> Recommendations provided for leading Doctors&#39; in respective field, easing the process of selection for patient and/or family members. Thus enabling an effective commencement of treatment</li>
					
					<li style="padding:  5px 0px 0px 0px;"><b>Doctor&#39;s appointment :</b> Once the Doctor is confirmed, ATN Medicare helps in getting the Doctor's appointment, further escort the patient to the clinic.</li>
					
					<li style="padding:  5px 0px 0px 0px;"><b>Ambulance:</b> On requirement of the Ambulance service, ATN Medicare makes arrangement for the same with a state of the art built in facilities for patient traveling from all the entry point of Kolkata to the hospital/nursing home.</li>
					
					<li style="padding:  5px 0px 0px 0px;"><b>Post-Operative Care and Support :</b> The patient are provided total care and support from ATN Medicare post operation / surgery such as visiting the doctor with reports, medicine procurement and provide the feedback to patients and accompanying members.</li>
					
					<li style="padding:  5px 0px 0px 0px;"><b>Extra assistance for physically challenged :</b> Extra care and assistance for the physically challenged / differently abled can be provided.</li>
					
					<li style="padding:  5px 0px 0px 0px;"><b>Private nursing attendance on request :</b> Private and special care nursing attendance can be availed by the patient on request basis.</li>
					
					<li style="padding: 5px 0px 0px 0px"><b>Follow up :</b> Travelling for routine post hospitalization reporting or for other issues within a few days is hectic and expensive. Hence ATN Medicare will take care of the follow up procedure. Video conference, online chat and so on between Doctors & Patients are facilitated/arranged for.</li>

					<li style="padding: 5px 0px 15px 0px"><b>Medicine arrangements :</b> Medicine arrangement is a matter of concern for overseas patients. ATN Medicare provides such aid to the patients in such procurement of the medicines.</li>
					
					</ul> 
			
			
			
			
			</p>
		
		</div>
 
 <!-- 
 		<div class="page-header" style="margin: 0;">
			<h2 class="text-success" style="font-size:1.8em; font-weight: bold">
				 Travel and Tourism
			</h2>
		</div>
				<div class="panel panel-default" style="border: none;">
					<ul style="list-style-type:square">
					<li style="padding: 5px 0px 0px 0px"><b>Ticketing (Bus, Air, Rail) :</b> ATN Medicare will take care of Ticketing arrangements for the patient and party entailing a hassle-free travel.</li>
					
					<li style="padding: 5px 0px 0px 0px"><b>Travel Services :</b> Tour and travel in and around Kolkata and across West Bengal for patient/party can be arranged if the party so feels as per their schedule.</li>
					<li style="padding: 5px 0px 0px 0px"><b>Station / Border Checkpost / Airport pick up & drop off :</b> ATN Medicare will welcome the patient / party and see-off at Station / Border Checkpost / Airport.</li>
					<li style="padding: 5px 0px 0px 0px"><b>Accommodation :</b> Accommodation will be arranged for patients and its family by Uposhom according to their needs and choice. There are plenty of options of accommodation at convenient locations and comfortable prices.</li>
					<li style="padding: 5px 0px 0px 0px"><b>Transportation :</b> Local travel and transportation services will be arranged by ATN Medicare.</li>
					<li style="padding: 5px 0px 0px 0px"><b>SIM Cards :</b> SIM card procurement for patient/party travelling in India will be processed.</li>
					<li style="padding: 5px 0px 0px 0px"><b>Bilingual Interpretation :</b> If need be, language interpreters are also provided.</li>
					<li style="padding:  5px 0px 15px 0px;"><b>Guide :</b> Guides for travel in cities for the patient and family.</li>
					</ul> 
			</p>
		
		</div>
		<div class="page-header" style="margin: 0;">
			<h2 class="text-success" style="font-size:1.8em; font-weight: bold">
				 Official and Legal
			</h2>
		</div>
				<div class="panel panel-default" style="border: none;">
					<ul style="list-style-type:square">
					<li style="padding: 5px 0px 0px 0px"><b>Passport Processing</b> </li>
					<li style="padding: 5px 0px 0px 0px"><b>VISA Processing</b> </li>
					<li style="padding: 5px 0px 0px 0px"><b>Arranging emergency insurance</b> </li>
					<li style="padding: 5px 0px 0px 0px"><b>Arranging travel insurance</b> </li>
					<li style="padding: 5px 0px 15px 0px"><b>Arranging health insurance</b> </li>
					
					</ul> 
			</p>
		
		</div>
		
		<div class="page-header" style="margin: 0;">
			<h2 class="text-success" style="font-size:1.8em; font-weight: bold">
				 Feedback
			</h2>
		</div>
		<div class="panel panel-default" style="border: none;">
		    <p class="lead" style="padding:15px 0px;font-size:1.3em">After the completion of the treatment of a patient, ATN Medicare&#39;s feedback team will talk to the patients &amp; will gather into reading the overall experience. The patient will have an option to rate Hospitals/Doctors/Pathological labs. 
</p>
		
		</div>
   -->

	</div>

	

	<footer class="footer navbar-fixed-bottom" style="border-top: 1px solid #E1E1E1; background-color: #EEEEEE;">
      <div class="container-fluid" style="display:table; margin-top:7px;padding-bottom: 0px;width:100%">
       <p class="lead pull-left" style="display:table-cell"><small>Copyright &copy; 2016</small></p>
        <p class="lead pull-right" style="display:table-cell"><small>ATN Medicare</small></p>
      </div>
    </footer>

</body>




</html>
