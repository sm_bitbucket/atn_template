<!DOCTYPE html>
<html lang="en">
<head>
 <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
 <title>ATN Medicare - Contact Us !!!</title>    
<link rel="stylesheet" type="text/css"
	href="./bootstrap/css/bootstrap-theme.min.css" />
<link rel="stylesheet" type="text/css"
	href="./bootstrap/css/bootstrap-theme.min.css.map" />
<link rel="stylesheet" type="text/css"
	href="./bootstrap/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css"
	href="./bootstrap/css/bootstrap.min.css.map" />
<link rel="stylesheet" type="text/css"
	href="./bootstrap/css/normalize.css" />
<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans" />

<script src="./bootstrap/js/jquery-2.2.3.min.js"></script>
<script src="./bootstrap/js/bootstrap.min.js"></script>

<script src="./bootstrap/js/require.js"></script>
<script src="./bootstrap/js/collapse.js"></script>
<script src="./bootstrap/js/dropdown.js"></script>
<script src="./bootstrap/js/transition.js"></script>

<style type="text/css">
html,body,h1,h2,h3,h4,h5,h6,p,span {
	font-family: 'Open Sans', sans-serif!important;
 	font-weight: 400!important;
}
.jumbotron {
	position: relative;
    background: #000 url("./images/contactus.jpg") no-repeat;
    background-size: 100% 100%;
    -moz-background-size: 100% 100%;
    -webkit-background-size: 100% 100%;
    -o-background-size: 100% 100%;
    height: 60vh;
        overflow: hidden;
        border-bottom: 1px solid #e1e1e1;
 	margin-bottom: 0;
}
#errmsg
{
color: red;
}
</style>


<script type="text/javascript">
$(document).ready(function () {
	  //called when key is pressed in textbox
	  $("#mobile").keypress(function (e) {
	     //if the letter is not digit then display error and don't type anything
	     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
	        //display error message
	        $("#errmsg").html("Numbers are allowed only").show().fadeOut("slow");
	               return false;
	    }
	   });
	});

function send(){
	
	var name = $('#name').val();
	var email = $('#email').val();
	var mobile = $('#mobile').val();
	var comment = $('#comment').val();
	
	if(name == ""){		
		$("#errmsg").html("Please provide your name!").show().fadeOut("slow");
		$('#name').focus();
        return false;
	}
	if(email == ""){
		
		$("#errmsg").html("Please provide your email!").show().fadeOut("slow");
		$('#email').focus();
        return false;
	}else{
		
		var atpos = email.indexOf("@");
		var dotpos = email.lastIndexOf(".");
		if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length) {
		    $("#errmsg").html("Not a valid e-mail!").show().fadeOut("slow");
			$('#email').focus();
	        return false;
		}
	}
	if(mobile == ""){
		$("#errmsg").html("Please provide your mobile!").show().fadeOut("slow");
		$('#mobile').focus();
        return false;
     }
	if(comment == ""){
		$("#errmsg").html("Please provide your comment!").show().fadeOut("slow");
		$('#comment').focus();
        return false;
	}
	
	var data = 'decn=3&name='+name+'&email='+email+'&mobile='+mobile+'&comment='+comment;
	
	// 'http://192.168.1.100:8082/webMedicare';   /';
	//'http://www.atnmedicare.com/webMedicare/GetClientsFullDetails';
	var url = 'http://localhost:8080/webMedicare/GetClientsFullDetails';	
	$.ajax({
		type: 'GET',
		async: false,
		url: url,
		data: data,
		crossDomain: true,
		/* processData: true, */
		/* dataType: 'jsonp', */
		success: function(resp){
			console.log(resp);
			if(resp == 1){
				clearFields();
			}
		}
	});
}


function clearFields(){
	$('#name').val('');
	$('#email').val('');
	$('#mobile').val('');
	$('#comment').val('');
	
	$('#close').click();
}
</script>

</head>


<body>
<header class="">
<nav class="navbar navbar-fixed-top" style="background-color: #FFF; margin-bottom: 0; border: 1px solid #E1E1E1;">
		<div class="container-fluid">

			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"
					aria-expanded="false">
					<span class="sr-only" style="background-color: #000;">Toggle navigation</span>
					 <span class="icon-bar" style="background-color: #000;"></span>
						 <span class="icon-bar" style="background-color: #000;"></span> 
						 <span class="icon-bar" style="background-color: #000;"></span>
				</button>


				<a class="navbar-brand" href="javascript:void(0);"
					style="margin: 0;"> <img alt="Brand"
					src="./images/MEDICARE1.png" style="margin: -12px;" width="120px"
					height="45px">

				</a>

			</div>

<div class="navbar-header" style="display:table; padding-top: 12px;padding-bottom: 6px;"><p class="lead" style="display:table-cell;padding-left:0.5em; font-size:1.3em;color: #fcd309;font-weight: bold"><font style="color:#EB2728">Medical</font> <font style="color:#1B8F48">Aid</font> ~ <font style="color:#2F2F79">Beyond </font>Barriers</p></div>
			<div class="navbar-right collapse navbar-collapse"
				id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav" style="color: #000">
					<li class="active"><a href="./home.jsp" class="text-success">Home </a></li>
					<li ><a href="./whyKolkata.jsp" class="text-success">Why Kolkata?</a></li>
					<li ><a href="./services.jsp" class="text-success">Services</a></li>
					<li class="active" style="font-weight:bold;color: #1d904a;"><a href="./contact_us.jsp" class="text-success">Contact us</a></li>
					<li><a href="http://atnmedicare.com/m/#/clientReg" class="text-success">Register</a></li>
					<li><a href="http://atnmedicare.com/m/#/login" class="text-success">Login</a></li>
				</ul>
			</div>
		</div>
	</nav>

</header>
	
</div>

	<div class="jumbotron">
		<!-- <div class="container text-center" style="background: #E1E1E1; bottom: 0; position: absolute;   margin: auto;    width: 100%;">
		
			<a class="btn btn-link btn-lg" href="#" role="button">Sign Up</a>
		
		</div> -->
		
	</div>
	<div class="container-fluid" style="margin-bottom: 4em;">

		<div class="page-header row" style="margin: 0;">
			<div class="col-xs-12 col-md-4 col-sm-4">
				<h2 class="text-success" style="font-size:1.8em; font-weight: bold">
				Reach us at your convenience
			</h2>
			</div>
			<div class="col-xs-12 col-md-8 col-sm-8" style="padding-top: 0.7em;">
		
				<button type="button" class="btn btn-lg btn-danger" data-toggle="modal" data-target="#cmodal" >Contact Us</button>
			
			</div>
		</div>
<div class="row">
		<p class="lead" style="font-size: 1.3em; padding-left: 1.8em;"> Currently we have four offices connecting three countries. Reach us at our Kolkata, Singapore or Dhaka office as per your convenience.
We shall shortly be opening offices at : -Chittagaon,Khulna,Pabna,Jessore,Sathkheera, Sylet.
		</p>
</div>

<div class="row" style="border-bottom: 1px solid #E1E1E1;">
	<div class="col-xs-12 col-md-6 col-sm-6 " >
			<ul style="margin-left: 0;    padding-left: 1.5em; list-style-type:square;">
		<li><b>Kolkata Office:</b></li>
			P-24, Darga Road Ground Floor Kolkata 70016<br> 
			Tel No. : 033 6450 9479 / 9748759479<br>
			WhatsApp +919836663763
<!--			
<br/>Email:- azharul@atntravel.com<br/>Website : <a href="http://www.atntravel.com" target="_blank" style="text-decoration: none; cursor: pointer;" >www.atntravel.com</a>
-->
			<br><br>
		</ul>
	
	</div>
	<div class="col-xs-12 col-md-6 col-sm-6" style="border-left: 1px solid #E1E1E1;">
	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3684.955532786509!2d88.36864771436541!3d22.543338439726625!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3a0276e64c538643%3A0x55e31bb7d200236!2s24%2C+Darga+Rd%2C+Beniapukur%2C+Kolkata%2C+West+Bengal+700017!5e0!3m2!1sen!2sin!4v1467203147420" frameborder="0" style="border:0; width: 100%; max-height: 400px; min-height: 300px; " allowfullscreen></iframe>

	</div>
	
</div>		   


<div class="row" style="border-bottom: 1px solid #E1E1E1;">
	<div class="col-xs-12 col-md-6 col-sm-6">
		<ul style="margin-left: 0;    padding-left: 1.5em; padding-top: 1em; list-style-type:square;">
		<li><b>Bangladesh Dhaka Office:</b> </li>
			<p style="padding:  0px 0px 15px 0px;">Dhaka Trade Centre <br>99,Kazi Nazrul Islam Avenue (3rd Floor)<br>Kawran Bazar,Dhaka-1218<br/>Bangladesh<br/>Tel No : +8801535 155208, /  +8804478082322<br/>Email:- azharul@atntravel.com<br/>Website : <a href="http://www.atntravel.com" target="_blank" style="text-decoration: none; cursor: pointer;" >www.atntravel.com</a></p>
		</ul>
	</div>
	<div class="col-xs-12 col-md-6 col-sm-6" style="border-left: 1px solid #E1E1E1;">
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3651.848829407856!2d90.39030731438842!3d23.75276959460401!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755b8a2ece75c55%3A0xf56a606816f68034!2s99%2C+Kazi+Nazrul+Islam+Avenue%2C+Dhaka+Trade+Center%2C+99+Kazi+Nazrul+Islam+Ave%2C+Dhaka+1215%2C+Bangladesh!5e0!3m2!1sen!2sin!4v1468222415591" frameborder="0" style="border:0; width: 100%; max-height: 400px; min-height: 300px; " allowfullscreen=""></iframe>
	</div>

</div>

<div class="row" style="border-bottom: 1px solid #E1E1E1;">
	<div class="col-xs-12 col-md-6 col-sm-6">
		<ul style="margin-left: 0;  padding-top: 1em;  padding-left: 1.5em; list-style-type:square;">
		<li><b>Petrapole Check Post</b> </li>
			<p  style="padding:  0px 0px 15px 0px;">Sohag Paribahan <br>Ph no  +913215245022 / +913215245752<br>
			Mo - +919775180344</p>
		</ul>
	</div>
	<div class="col-xs-12 col-md-6 col-sm-6" style="border-left: 1px solid #E1E1E1;">
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3671.603795360743!2d88.88114331448911!3d23.03831532152177!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39ff36675016e6ad%3A0x53aab00a75e4fadd!2sPetrapole+Check+Post!5e0!3m2!1sen!2sin!4v1467889330384" frameborder="0" style="border:0; width: 100%; max-height: 400px; min-height: 300px; " allowfullscreen></iframe>

	</div>

</div>			   
		
<div class="row" style="border-bottom: 1px solid #E1E1E1;">
	<div class="col-xs-12 col-md-6 col-sm-6">
		<ul style="margin-left: 0;  padding-top: 1em;  padding-left: 1.5em; list-style-type:square;">
		<li><b>Singapore  Office:</b> </li>
			<p style="padding:  0px 0px 15px 0px;">320 Serangoon Road<br># 04-25,Serangoon Plaza<br>Singapore : 218108<br/>Tel No : (65) 6292 7153<br/>Fax No : ( 65) 6293 2590<br/>Email:- azharul@atntravel.com<br/>Website : <a href="http://www.atntravel.com" target="_blank" style="text-decoration: none; cursor: pointer;" >www.atntravel.com</a></p>
		</ul>
	</div>
	<div class="col-xs-12 col-md-6 col-sm-6" style="border-left: 1px solid #E1E1E1;">
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3988.77408716875!2d103.85304221419173!3d1.3108991620658899!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31da19c639400001%3A0x70c077578edd147c!2sSerangoon+Plaza!5e0!3m2!1sen!2sin!4v1468221385939" frameborder="0" style="border:0; width: 100%; max-height: 400px; min-height: 300px; " allowfullscreen=""></iframe>

	</div>

</div>



</div>
 
 <!-- Contact Modal -->
  <div class="modal fade" id="cmodal" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Contact Us</h4>
          <span id="errmsg" style="clear: both;"></span>
        </div>
        <div class="modal-body">
          	<div class="form-group">
			  <label for="usr">Name:</label>
			  <input type="text" class="form-control" id="name">
			</div>
			<div class="form-group">
			  <label for="pwd">Email:</label>
			  <input type="text" class="form-control" id="email">
			</div>
			<div class="form-group">
			  <label for="pwd">Mobile:</label>
			  <input type="text" class="form-control" id="mobile">
			</div>
			<div class="form-group">
			  <label for="pwd">Comment:</label>
				<textarea class="form-control" rows="5" id="comment"></textarea>
			</div>
        </div>
        <div class="modal-footer">
          <button type="button" onclick="send();" class="btn btn-primary pull-left" >Send</button>
          <button type="button" id="close" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>


	

	<footer class="footer navbar-fixed-bottom" style="border-top: 1px solid #E1E1E1; background-color: #EEEEEE;">
      <div class="container-fluid" style="display:table; margin-top:7px;padding-bottom: 0px;width:100%">
       <p class="lead pull-left" style="display:table-cell"><small>Copyright &copy; 2016</small></p>
        <p class="lead pull-right" style="display:table-cell"><small>ATN Medicare</small></p>
      </div>
    </footer>
</body>



</html>
