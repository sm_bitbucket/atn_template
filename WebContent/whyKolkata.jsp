<!DOCTYPE html>
<html lang="en">
<head>
 <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
 <title>ATN Medicare - Why Kolkata !!!</title>    
<link rel="stylesheet" type="text/css"
	href="./bootstrap/css/bootstrap-theme.min.css" />
<link rel="stylesheet" type="text/css"
	href="./bootstrap/css/bootstrap-theme.min.css.map" />
<link rel="stylesheet" type="text/css"
	href="./bootstrap/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css"
	href="./bootstrap/css/bootstrap.min.css.map" />
<link rel="stylesheet" type="text/css"
	href="./bootstrap/css/normalize.css" />
<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans" />
<script src="./bootstrap/js/jquery-2.2.3.min.js"></script>
<script src="./bootstrap/js/bootstrap.min.js"></script>

<script src="./bootstrap/js/require.js"></script>
<script src="./bootstrap/js/collapse.js"></script>
<script src="./bootstrap/js/dropdown.js"></script>
<script src="./bootstrap/js/transition.js"></script>

<style type="text/css">
html,body,h1,h2,h3,h4,h5,h6,p,span {
	font-family: 'Open Sans', sans-serif!important;
 	font-weight: 400!important;
}
.jumbotron {
	position: relative;
    /* background: #000 url("./images/landscape.jpg") center center; */
     background: #000 url("./images/a.jpg") center center; 
    background-size: 100% 100%;
    -moz-background-size: 100% 100%;
    -webkit-background-size: 100% 100%;
    -o-background-size: 100% 100%;
    height: 350px;
        overflow: hidden;
        border-bottom: 1px solid #e1e1e1;
 	margin-bottom: 0;
}

</style>




</head>

<body>
<header class="">
<nav class="navbar navbar-fixed-top" style="background-color: #FFF; margin-bottom: 0; border: 1px solid #E1E1E1;">
		<div class="container-fluid">

			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"
					aria-expanded="false">
					<span class="sr-only" style="background-color: #000;">Toggle navigation</span>
					 <span class="icon-bar" style="background-color: #000;"></span>
						 <span class="icon-bar" style="background-color: #000;"></span> 
						 <span class="icon-bar" style="background-color: #000;"></span>
				</button>


				<a class="navbar-brand" href="javascript:void(0);"
					style="margin: 0;"> <img alt="Brand"
					src="./images/MEDICARE1.png" style="margin: -12px;" width="120px"
					height="45px">

				</a>

			</div>
<div class="navbar-header" style="display:table; padding-top: 12px;padding-bottom: 6px;"><p class="lead" style="display:table-cell;padding-left:0.5em; font-size:1.3em;color: #fcd309;font-weight: bold"><font style="color:#EB2728">Medical</font> <font style="color:#1B8F48">Aid</font> ~ <font style="color:#2F2F79">Beyond </font>Barriers</p></div>
			<div class="navbar-right collapse navbar-collapse"
				id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav" style="color: #000">
					<li class="active"><a href="./home.jsp" class="text-success">Home </a></li>
					<li class="active" style="font-weight:bold;color: #1d904a;"><a href="./whyKolkata.jsp" class="text-success">Why Kolkata?</a></li>
					<li><a href="./services.jsp" class="text-success">Services</a></li>
					<li><a href="./contact_us.jsp" class="text-success">Contact us</a></li>
					<li><a href="http://atnmedicare.com/m/#/clientReg" class="text-success">Register</a></li>
					<li><a href="http://atnmedicare.com/m/#/login" class="text-success">Login</a></li>
				</ul>
			</div>
		</div>
	</nav>

</header>
	


	<div class="jumbotron">
		<!-- <div class="container text-center" style="background: #E1E1E1; bottom: 0; position: absolute;   margin: auto;    width: 100%;">
		
			<a class="btn btn-link btn-lg" href="#" role="button">Sign Up</a>
		
		</div> -->
		
	</div>
	<div class="container-fluid" style="margin-bottom: 4em;">

<div class="page-header" style="margin: 0;">
			<h1 class="text-primary"></h1>
			<h2 class="text-success" style="font-size:1.8em; font-weight: bold">
				 Kolkata - A dream destination for Bangladesh Patients
			</h2>
		</div>

		<div class="panel panel-default" style="border: none;">
		    <p class="lead" style="padding:15px 0px 0px 0px;font-size:1.3em">Owing to cultural and linguistic similarities, Bangladeshi patients are more comfortable in getting the treatment done in Kolkata.</p>
		<p class="lead" style="padding:  0px 0px 0px 0px; font-size:1.3em">There are several other reasons that include
					<ul style="list-style-type:square"><li>Highly skilled and well updated healthcare professionals</li>
					
					<li>Availability of latest and updated medical technologies, along with all compliance adhering to the standards of international quality and performance.</li>
					
					<li>Significantly reduced costs without compromising on quality because of highly subsidized tax structure.</li>
					
					<li style="padding:  0px 0px 15px 0px;">Presence of JCI/JCHO, NABH accredited or internationally acclaimed and certified hospitals.</li>
					
				<!-- 	<li style="padding:  0px 0px 15px 0px;">Lesser visa restrictions and introduction of a visa-on-arrival scheme by the Indian Government for medical tourists from select countries including Bangladesh.</li> -->
				</ul> 
</p>
		
		</div>
 
 <!-- Login Modal -->
  <div class="modal fade" id="loginmodal" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Login</h4>
        </div>
        <div class="modal-body">
          	<div class="form-group">
			  <label for="usr">Username:</label>
			  <input type="text" class="form-control" id="usr">
			</div>
			<div class="form-group">
			  <label for="pwd">Password:</label>
			  <input type="password" class="form-control" id="pwd">
			</div>
        </div>
        <div class="modal-footer">
          <button type="button" onclick="login();" class="btn btn-primary pull-left" >Sign in</button>
          <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

	</div>


	<footer class="footer navbar-fixed-bottom" style="border-top: 1px solid #E1E1E1; background-color: #EEEEEE;">
      <div class="container-fluid" style="display:table; margin-top:7px;padding-bottom: 0px;width:100%">
       <p class="lead pull-left" style="display:table-cell"><small>Copyright &copy; 2016</small></p>
        <p class="lead pull-right" style="display:table-cell"><small>ATN Medicare</small></p>
      </div>
    </footer>

</body>




</html>